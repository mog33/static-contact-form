CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Static Contact form transform Drupal core Contact forms as static Html Contact
forms managed by a third party service.
This is usefull when your hosting do not permit to send mail or you are using
Tome module to generate a static version of your website.

REQUIREMENTS
------------

Drupal core Contact enable.


RECOMMENDED MODULES
-------------------

 * Tome(https://www.drupal.org/project/tome):
   Create a static HTML version of your website.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

Got to your feedback form, enable third party service and follow instructions.


MAINTAINERS
-----------

Current maintainers:
 * Jean Valverde (mogtofu33) - https://www.drupal.org/u/mogtofu33
