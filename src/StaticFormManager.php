<?php

namespace Drupal\static_contact_form;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Static form plugin manager.
 */
class StaticFormManager extends DefaultPluginManager {

  /**
   * Constructs a new StaticFormManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/StaticForm', $namespaces, $module_handler, 'Drupal\static_contact_form\Plugin\StaticFormPluginInterface', 'Drupal\static_contact_form\Annotation\StaticForm');

    $this->alterInfo('static_contact_form_info');
    $this->setCacheBackend($cache_backend, 'static_contact_form_plugins');
  }

}
