<?php

namespace Drupal\static_contact_form\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Static form item annotation object.
 *
 * @see \Drupal\static_contact_form\StaticFormManager
 * @see \Drupal\static_contact_form\Plugin\StaticFormPluginBase
 * @see \Drupal\static_contact_form\Plugin\StaticFormPluginInterface
 * @see plugin_api
 *
 * @Annotation
 */
class StaticForm extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The url of the plugin service.
   *
   * @var string
   */
  public $url;

}
