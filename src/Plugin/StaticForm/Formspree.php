<?php

namespace Drupal\static_contact_form\Plugin\StaticForm;

use Drupal\static_contact_form\Plugin\StaticFormPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Add FormSpree support For Static Contact Form module.
 *
 * @StaticForm(
 *  id = "formspree",
 *  label = @Translation("FormSpree"),
 *  url = "https://formspree.io"
 * )
 */
class Formspree extends StaticFormPluginBase {

  /**
   * {@inheritdoc}
   */
  public function help() {
    $output = '<h3>' . $this->t('Instructions') . '</h3>';
    $output .= '<p>' . $this->t('Use free plan without registration from <a href="@url">Formspree</a>.', ['@url' => $this->url()]) . '</p>';
    $output .= '<p>' . $this->t('Set a mail and save this form. Then you need to use the contact form the first time to complete registration.') . '</p>';
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Formspree email'),
      '#size' => 35,
      '#description' => $this->t('Fill the <em>email</em> used to receive mails from Formspree.'),
      '#default_value' => isset($this->settings['mail']) ? $this->settings['mail'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function alterContactMessageForm(array &$form, FormStateInterface $form_state, $entity) {
    parent::alterContactMessageForm($form, $form_state, $entity);

    // Set action to Formspree.
    $form['#action'] = 'https://formspree.io/' . $this->settings['mail'];
    $form['message']['widget'][0]['value']['#attributes']['name'] = 'message';
    $form['mail']['#attributes']['name'] = '_replyto';
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $values = $form_state->getValue('static_contact_form');

    if ($values['service'] !== $this->id()) {
      return;
    }

    $mail = $values[$this->id()]['mail'];
    if (empty($mail)) {
      $form_state->setErrorByName('static_contact_form][' . $this->id() . '][mail', $this->t('The mail is required.'));
      return;
    }
  }

}
