<?php

namespace Drupal\static_contact_form\Plugin\StaticForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\static_contact_form\Plugin\StaticFormPluginBase;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use GuzzleHttp\Exception\RequestException;

/**
 * Add Google Form for Contact form.
 *
 * Inspired by https://blog.webjeda.com/google-form-customize/
 *
 * @StaticForm(
 *  id = "googleform",
 *  label = @Translation("Google Form"),
 *  url = "https://docs.google.com/forms",
 * )
 */
class Googleform extends StaticFormPluginBase {

  /**
   * {@inheritdoc}
   */
  public function help() {
    $output = '<h3>' . $this->t('Instructions') . '</h3>';
    $output .= '<p>' . $this->t('Create a form at <a href="@url">Google form</a> with email collection and a field name and message.', ['@url' => $this->url()]) . '</p>';
    $output .= '<p>' . $this->t('Click the eye to view your form at Google, paste the url here (https://docs.google.com/forms/d/e/xxxx/viewform) and save.') . '</p>';
    $output .= '<p>' . $this->t('This will to extract the fields values to match the Drupal Form. Only name, email and message fields are currently supported.') . '</p>';
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Form url'),
      '#size' => 100,
      '#default_value' => isset($this->settings['url']) ? $this->settings['url'] : '',
    ];

    $form['redirect'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Redirect submission to this website'),
      '#description' => $this->t('To use this feature you must copy the template file <em>form--contact-message-googleform.html.twig</em> from this module to your theme.'),
      '#default_value' => isset($this->settings['redirect']) ? $this->settings['redirect'] : '',
    ];

    if (isset($this->settings['data'])) {
      $form['data'] = [
        '#type' => 'details',
        '#open' => FALSE,
        '#title' => $this->t('Form values extracted'),
        'data' => [
          '#markup' => isset($this->settings['data']) ? '<pre>' . print_r($this->settings['data'], TRUE) . '</pre>' : '',
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (!$values = $form_state->getValue('static_contact_form')[$this->id()]) {
      return;
    }

    if (empty($values['url'])) {
      $form_state->setErrorByName('static_contact_form][' . $this->id() . '][url', $this->t('The url is required.'));
      return;
    }

    try {
      $response = \Drupal::httpClient()->get($values['url'], ['headers' => ['Accept' => 'text/plain']]);
      if ($response->getStatusCode() != 200) {
        $form_state->setErrorByName('static_contact_form][' . $this->id() . '][url', $this->t('The validation of "@url" failed with error "@error" (HTTP code @code).', [
          '@url' => UrlHelper::filterBadProtocol($values['url']),
          '@error' => $response->getReasonPhrase(),
          '@code' => $response->getStatusCode(),
        ]));
      }
      else {
        $data = (string) $response->getBody();
        if (empty($data)) {
          $form_state->setErrorByName('static_contact_form][' . $this->id() . '][url', $this->t('The validation of "@url" failed with empty content.', [
            '@url' => UrlHelper::filterBadProtocol($values['url']),
          ]));
        }
        else {
          // Save the form as a state for convenience and later process.
          $state = \Drupal::state();
          $state->set('static_contact_form.googleform', $data);
          $this->messenger()->addMessage($this->t('Google form processed.'));
        }
      }
    }
    catch (RequestException $e) {
      $form_state->setErrorByName('static_contact_form', $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function alterValues(array &$values) {
    $state = \Drupal::state();
    if (!$data = $state->get('static_contact_form.googleform')) {
      return;
    }

    $google_form = Html::load($data);
    $elements = [];

    $elements['action'] = $google_form->getElementsByTagName('form')->item(0)->getAttribute('action');

    foreach ($google_form->getElementsByTagName('input') as $input) {
      if ($input->getAttribute('type') == 'email') {
        $elements['email'][] = [$input->getAttribute('name'), $input->getAttribute('aria-label')];
      }
      if ($input->getAttribute('type') == 'text') {
        $elements['text'][] = [$input->getAttribute('name'), $input->getAttribute('aria-label')];
      }
      if ($input->getAttribute('type') == 'select') {
        $elements['select'][] = [$input->getAttribute('name'), $input->getAttribute('aria-label')];
      }
    }

    foreach ($google_form->getElementsByTagName('textarea') as $textarea) {
      $elements['message'][] = [$textarea->getAttribute('name'), $textarea->getAttribute('aria-label')];
    }

    $values['googleform']['data'] = $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function alterContactMessageForm(array &$form, FormStateInterface $form_state, $entity) {
    parent::alterContactMessageForm($form, $form_state, $entity);

    if ($this->currentUser->hasPermission('view static form submissions link')) {
      $this->messenger->addMessage($this->t('Access <a href="@url">submissions of this form</a>.', ['@url' => $this->url()]));
    }

    // Replace by google form values.
    $form['#action'] = $this->settings['data']['action'];

    if ($this->settings['redirect']) {
      if ($entity->getRedirectUrl()) {
        $form['#attributes']['target'] = 'hidden_iframe';
        $form['#attributes']['onsubmit'] = 'submitted=true;';
        $form['redirect_to'] = [
          '#type' => 'hidden',
          '#value' => $entity->getRedirectUrl()->setAbsolute()->toString(),
        ];
      }
    }

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#attributes' => [
        'name' => isset($this->settings['data']['text'][0][0]) ? $this->settings['data']['text'][0][0] : '',
      ],
    ];
    $form['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail'),
      '#attributes' => [
        'name' => isset($this->settings['data']['email'][0][0]) ? $this->settings['data']['email'][0][0] : '',
      ],
    ];
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#attributes' => [
        'name' => isset($this->settings['data']['message'][0][0]) ? $this->settings['data']['message'][0][0] : '',
      ],
    ];
  }

}
