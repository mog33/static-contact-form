<?php

namespace Drupal\static_contact_form\Plugin\StaticForm;

use Drupal\static_contact_form\Plugin\StaticFormPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Add Simpleform support For Static Contact Form module.
 *
 * @StaticForm(
 *  id = "simpleform",
 *  label = @Translation("Simple Form"),
 *  url = "https://getsimpleform.com",
 * )
 */
class Simpleform extends StaticFormPluginBase {

  /**
   * {@inheritdoc}
   */
  public function help() {
    $output = '<h3>' . $this->t('Instructions') . '</h3>';
    $output .= '<p>' . $this->t('Register at <a href="@url">Simpleform</a> and fill the api token and form api token.', ['@url' => $this->url()]) . '</p>';
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    if (isset($this->settings['api_token'])) {
      $form['help'] = [
        '#theme' => 'item_list',
        '#items' => [
          $this->t('Form instructions (only akismet part is needed for spam protection) <a href="@url">here</a>.', ['@url' => $this->url() . '/instructions?api_token=' . $this->settings['api_token']]),
          $this->t('Add spam support and configure your form <a href="@url">here</a>.', ['@url' => $this->url() . '/forms/' . $this->settings['api_token'] . '/edit']),
          $this->t('Access your submissions form <a href="@url">here</a>.', ['@url' => $this->url() . '/messages?api_token=' . $this->settings['api_token']]),
        ],
      ];
    }

    $form['form_api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form api token'),
      '#size' => 35,
      '#description' => $this->t('Fill the <em>form api token</em> from Simpleform.'),
      '#default_value' => isset($this->settings['form_api_token']) ? $this->settings['form_api_token'] : '',
    ];

    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Api token'),
      '#size' => 35,
      '#description' => $this->t('Fill the <em>api token</em> from Simpleform.'),
      '#default_value' => isset($this->settings['api_token']) ? $this->settings['api_token'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function alterContactMessageForm(array &$form, FormStateInterface $form_state, $entity) {
    parent::alterContactMessageForm($form, $form_state, $entity);

    if ($this->currentUser->hasPermission('view static form submissions link')) {
      $this->messenger->addMessage($this->t('Access <a href="@url">submissions of this form</a>.', ['@url' => $this->url() . '/messages?api_token=' . $this->settings['api_token']]));
    }

    // Change message name for Akismet support.
    $form['message']['widget'][0]['value']['#attributes']['name'] = 'message';
    // Set action to Simpleform.
    $form['#action'] = 'https://getsimpleform.com/messages?form_api_token=' . $this->settings['form_api_token'];
    // Add redirect path if exist.
    if ($entity->getRedirectPath()) {
      $form['redirect_to'] = [
        '#type' => 'hidden',
        '#value' => $entity->getRedirectPath(),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $values = $form_state->getValue('static_contact_form');

    if ($values['service'] !== $this->id()) {
      return;
    }

    $form_api_token = $values[$this->id()]['form_api_token'];
    if (empty($form_api_token)) {
      $form_state->setErrorByName('static_contact_form][' . $this->id() . '][form_api_token', $this->t('The form api token is required.'));
    }
    $api_token = $values[$this->id()]['api_token'];
    if (empty($api_token)) {
      $form_state->setErrorByName('static_contact_form][' . $this->id() . '][api_token', $this->t('The api token is required.'));
    }
    if (empty($form_api_token) || empty($api_token)) {
      return;
    }
  }

}
