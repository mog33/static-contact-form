<?php

namespace Drupal\static_contact_form\Plugin;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\contact\Entity\ContactForm;

/**
 * Base class for Static form plugins.
 *
 * @see \Drupal\static_contact_form\Annotation\StaticForm
 * @see \Drupal\static_contact_form\StaticFormManager
 * @see \Drupal\static_contact_form\Plugin\StaticFormPluginInterface
 * @see plugin_api
 */
abstract class StaticFormPluginBase extends PluginBase implements StaticFormPluginInterface {
  use MessengerTrait;

  /**
   * Plugin instance settings.
   *
   * @var array
   */
  public $settings = [];

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\Component\Plugin\StaticFormPluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountProxy $currentUser
   *   The account of the current user.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, AccountProxy $currentUser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setSettings($object) {
    if ($object instanceof ContactForm) {
      $this->settings = $object->getThirdPartySetting('static_contact_form', $this->id(), []);
    }
    elseif ($object instanceof FormStateInterface) {
      $entity = $object->getFormObject()->getEntity();
      if ($entity instanceof ThirdPartySettingsInterface) {
        $this->settings = $entity->getThirdPartySetting('static_contact_form', $this->id(), []);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function help() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->setSettings($form_state);
    $form = $this->usage();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function alterValues(array &$values) {
  }

  /**
   * {@inheritdoc}
   */
  public function alterContactMessageForm(array &$form, FormStateInterface $form_state, ContactForm $entity) {
    $this->setSettings($entity);

    // Add a service id flag on the form for template suggestion.
    $form['static_contact_form_service'] = [
      '#type' => 'hidden',
      '#value' => $this->id(),
    ];

    // Hide unused fields from Contact module.
    if (isset($form['actions']['preview'])) {
      $form['actions']['preview']['#access'] = FALSE;
    }

    if (isset($form['copy'])) {
      $form['copy']['#access'] = FALSE;
    }

    // Announce the alteration of the form by the service.
    if ($this->currentUser->hasPermission('administer static form')) {
      $this->messenger()->addMessage($this->t('This form is managed by third party service <b>@service</b>.', ['@service' => $this->label()]), 'warning');
    }
  }

  /**
   * Provide the id of a static form plugin.
   *
   * @return string
   *   A string description of the static form plugin.
   */
  protected function id() {
    return $this->getPluginId();
  }

  /**
   * Provide a name of the static form plugin.
   *
   * @return string
   *   A string name of the static form plugin.
   */
  protected function label() {
    return $this->pluginDefinition['label'];
  }

  /**
   * Provide the url of the static form plugin.
   *
   * @return string
   *   A string url of the static form plugin.
   */
  protected function url() {
    return $this->pluginDefinition['url'];
  }

  /**
   * Provide a description of the sandwich.
   *
   * @return string
   *   A string description of the sandwich.
   */
  protected function usage() {
    return [
      '#type' => 'fieldset',
      'help_note' => [
        '#markup' => $this->help(),
        '#weight' => 0,
      ],
      '#states' => [
        'visible' => [
          ':input[name="static_contact_form[use_service]"]' => ['checked' => TRUE],
          ':input[name="static_contact_form[service]"]' => ['value' => $this->id()],
        ],
      ],
    ];
  }

  /**
   * Provide a description of the sandwich.
   *
   * @return string
   *   A string description of the sandwich.
   */
  protected function default(FormStateInterface $form_state) {
    $form_object = $form_state->getFormObject();
    $entity = $form_object->getEntity();
    if (!$entity instanceof ThirdPartySettingsInterface) {
      return [];
    }
    return $entity->getThirdPartySetting('static_contact_form', $this->id(), []) + ['entity' => $entity];
  }

}
