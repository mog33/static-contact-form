<?php

namespace Drupal\static_contact_form\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\contact\Entity\ContactForm;

/**
 * Defines an interface for static form implementations.
 *
 * @see \Drupal\static_contact_form\StaticFormManager
 * @see plugin_api
 */
interface StaticFormPluginInterface extends ContainerFactoryPluginInterface {

  /**
   * Provide a note describing the service usage.
   *
   * @return string
   *   A string description of the service usage.
   */
  public function help();

  /**
   * Provide a note describing the service usage.
   *
   * @param \Drupal\Core\Form\FormStateInterface|\Drupal\contact\Entity\ContactForm $object
   *   The object to grab third party settings from.
   */
  public function setSettings($object);

  /**
   * Provide a form for settings of the static form service.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The Drupal form to store as third party settings.
   */
  public function buildForm(array $form, FormStateInterface $form_state);

  /**
   * Validate the form to integrate the service conditions.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state);

  /**
   * Process form values if needed for a service.
   *
   * @param array $values
   *   The form saved values to save as third party settings.
   */
  public function alterValues(array &$values);

  /**
   * Alter the Contact form to integrate the service rules.
   *
   * @param array $form
   *   The Drupal form to alter.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\contact\Entity\ContactForm $entity
   *   The Contact form entity.
   */
  public function alterContactMessageForm(array &$form, FormStateInterface $form_state, ContactForm $entity);

}
