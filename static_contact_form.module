<?php

/**
 * @file
 * Contains static_contact_form.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\contact\MessageInterface;
use Drupal\static_contact_form\Plugin\StaticFormPluginBase;

/**
 * Implements hook_help().
 */
function static_contact_form_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the static_contact_form module.
    case 'help.page.static_contact_form':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Add static contact form third services support for Drupal contact module.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Adds custom configuration to the contact edit form.
 *
 * @param array $form
 *   The form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 */
function static_contact_form_form_contact_form_edit_form_alter(array &$form, FormStateInterface $form_state) {
  $form_object = $form_state->getFormObject();

  if (!$form_object instanceof EntityFormInterface) {
    return;
  }

  $entity = $form_object->getEntity();
  if (!$entity instanceof ThirdPartySettingsInterface) {
    return;
  }

  $form['#tree'] = TRUE;

  // Add our main form wrapper.
  $form['static_contact_form'] = [
    '#type' => 'container',
  ];

  $form['static_contact_form']['use_service'] = [
    '#type' => 'checkbox',
    '#title' => t('Use third party static form service'),
    '#description' => t('When enabled, form will be transformed to be used as a static Html form with third party service.'),
    '#default_value' => $entity->getThirdPartySetting('static_contact_form', 'use_service', FALSE),
    '#weight' => -2,
  ];

  // Get the list of all the static form plugins and add specific form.
  $static_forms = \Drupal::service('plugin.manager.static_form');

  $options = [];

  foreach ($static_forms->getDefinitions() as $plugin_definition) {
    $options[$plugin_definition['id']] = $plugin_definition['label'];
    $instance = $static_forms->createInstance($plugin_definition['id']);
    if ($instance instanceof StaticFormPluginBase) {
      // Each subform of settings is added, weight put it under service below.
      $form['static_contact_form'][$plugin_definition['id']] = [];
      $subform_state = SubformState::createForSubform($form['static_contact_form'][$plugin_definition['id']], $form, $form_state);
      $form['static_contact_form'][$plugin_definition['id']] = $instance->buildForm($form['static_contact_form'][$plugin_definition['id']], $subform_state);
    }
  }

  $form['static_contact_form']['service'] = [
    '#type' => 'select',
    '#title' => t('Select the third party service'),
    '#options' => $options,
    '#description' => t('Note that <em>auto-reply</em> will not work and a <em>redirect path</em> is recommended.'),
    '#default_value' => $entity->getThirdPartySetting('static_contact_form', 'service', FALSE),
    '#states' => [
      'visible' => [
        ':input[name="static_contact_form[use_service]"]' => ['checked' => TRUE],
      ],
    ],
    '#weight' => -1,
  ];

  $form['#entity_builders'][] = 'static_contact_form_form_builder';
  $form['#validate'][] = 'static_contact_form_form_validate';
}

/**
 * Sets third party settings on contact forms.
 *
 * @param string $entity_type
 *   An entity type ID.
 * @param \Drupal\Core\Config\Entity\ThirdPartySettingsInterface $entity
 *   The entity being built.
 * @param array $form
 *   The form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 */
function static_contact_form_form_builder($entity_type, ThirdPartySettingsInterface $entity, array &$form, FormStateInterface $form_state) {
  if ($values = $form_state->getValue('static_contact_form')) {

    if ($values['use_service']) {
      $instance = \Drupal::service('plugin.manager.static_form')->createInstance($values['service']);
      if ($instance instanceof StaticFormPluginBase) {
        $instance->alterValues($values);
      }
    }

    // Save our values as a Third Party Setting on the Entity.
    foreach ($values as $key => $value) {
      $entity->setThirdPartySetting('static_contact_form', $key, $value);
    }
  }
}

/**
 * Add Static form service validation on contact forms.
 *
 * @param array $form
 *   The form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 */
function static_contact_form_form_validate(array &$form, FormStateInterface $form_state) {
  if ($values = $form_state->getValue('static_contact_form')) {
    if ($values['use_service']) {
      $instance = \Drupal::service('plugin.manager.static_form')->createInstance($values['service']);
      if ($instance instanceof StaticFormPluginBase) {
        $instance->validateForm($form, $form_state);
      }
    }
  }
}

/**
 * Adds elements and attributes to the given form.
 *
 * @param array $form
 *   The form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 */
function static_contact_form_form_contact_message_form_alter(array &$form, FormStateInterface $form_state) {

  $form_object = $form_state->getFormObject();
  if (!$form_object instanceof EntityFormInterface) {
    return;
  }

  $entity = $form_object->getEntity();
  if (!$entity instanceof MessageInterface) {
    return;
  }

  $contact_form = $entity->getContactForm();

  if (!$contact_form->getThirdPartySetting('static_contact_form', 'use_service', FALSE)) {
    return;
  }

  $service = $contact_form->getThirdPartySetting('static_contact_form', 'service', NULL);

  if (!$instance = \Drupal::service('plugin.manager.static_form')->createInstance($service)) {
    return;
  }

  if ($instance instanceof StaticFormPluginBase) {
    // Alter the form to match what we need for this service.
    $instance->alterContactMessageForm($form, $form_state, $contact_form);
  }
}

/**
 * Implements hook_theme_suggestions_views_exposed_form_alter().
 */
function static_contact_form_theme_suggestions_form_alter(array &$suggestions, array $variables) {
  // Add form suggestions based on type for contact.
  if (strpos($variables['element']['#id'], 'contact-message') !== FALSE) {
    $suggestions[] = 'form__contact_message';
    if (isset($variables['element']['static_contact_form_service'])) {
      $suggestions[] = 'form__contact_message_' . $variables['element']['static_contact_form_service']['#value'];
    }
    $suggestions[] = 'form__' . str_replace('-', '_', $variables['element']['#id']);
  }
}
